import React from 'react';
import './App.css';
import Todo from "./components/Todo";
import { Navbar, Nav, Container, Row, Col, Button, Input } from 'react-bootstrap';

function App(props) {
  return (
    <div className="todoapp stack-large">
      <Container fluid="lg" style={{backgroundColor: '#13293D', marginLeft:-10, marginRight: -10}}>
        <h1 id="title">Todo List</h1> 
      </Container>
      <form>
        <h2 className="label-wrapper">
          <label htmlFor="new-todo-input" className="label__lg">
            Organize your task better
          </label>
        </h2>
        <button type="submit" className="btn btn__primary btn__md"  style={{backgroundColor: '#13293D'}}>
          New Task
        </button>
        <br/>
      <Container>
        <Row>
          <Col xs={6}>
            <input type="text" id="new-todo-input" className="input" name="text" autoComplete="off"
        /></Col>
        <Col xs={4}><input
            type="text"
            id="new-todo-input"
            className="input"
            name="text"
            autoComplete="off"
        /></Col>
          <Col xs={2}><Button></Button></Col>
        </Row>
      </Container>

      </form>
      <div className="filters btn-group stack-exception">
        <button type="button" className="btn toggle-btn" aria-pressed="true">
          <span className="visually-hidden">Show </span>
          <span>all</span>
          <span className="visually-hidden"> tasks</span>
        </button>
        <button type="button" className="btn toggle-btn" aria-pressed="false">
          <span className="visually-hidden">Show </span>
          <span>Active</span>
          <span className="visually-hidden"> tasks</span>
        </button>
        <button type="button" className="btn toggle-btn" aria-pressed="false">
          <span className="visually-hidden">Show </span>
          <span>Completed</span>
          <span className="visually-hidden"> tasks</span>
        </button>
      </div>
      <h2 id="list-heading">
        3 tasks remaining
      </h2>
      <ul
        role="list"
        className="todo-list stack-large stack-exception"
        aria-labelledby="list-heading"
      >
        <Todo name="Eat" completed={true} id="todo-0" />
        <Todo name="Sleep" completed={false} id="todo-1" />
        <Todo name="Repeat" completed={false} id="todo-2" />
        <Todo name="Repeat" completed={false} id="todo-3" />
        <Todo name="Repeat" completed={false} id="todo-4" />
        <Todo name="Repeat" completed={false} id="todo-5" />
      </ul>
    </div>
  );
}

export default App;
